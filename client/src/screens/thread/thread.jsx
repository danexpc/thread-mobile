import * as React from 'react';
import {
  HomeScreenName,
  IconName,
  NotificationMessage,
  TextVariant
} from 'common/enums/enums';
import {
  Post,
  FlatList,
  Icon,
  Switch,
  Text,
  View
} from 'components/components';
import { AppColor } from 'config/config';
import { sharePost } from 'helpers/helpers';
import {
  useCallback,
  useDispatch,
  useEffect,
  useNavigation,
  useRef,
  useSelector,
  useState
} from 'hooks/hooks';
import RNFetchBlob from 'rn-fetch-blob';
import { Platform } from 'react-native';
import RNFS from 'react-native-fs';
import CameraRoll from '@react-native-community/cameraroll';
import { threadActionCreator } from 'store/actions';
import { notification as notificationService } from 'services/services';
import styles from './styles';
import { ActionType } from '../../store/thread/common';

const postsFilter = {
  userId: undefined,
  isLiked: undefined,
  from: 0,
  count: 10
};

const { dirs } = RNFetchBlob.fs;
const templatePath = Platform.OS === 'ios'
  ? `${dirs.MainBundleDir}/{imgName}`
  : `${RNFS.DownloadDirectoryPath}/{imgName}`;

const Thread = () => {
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const { posts, hasMorePosts, userId, updating } = useSelector(state => ({
    posts: state.posts.posts,
    hasMorePosts: state.posts.hasMorePosts,
    userId: state.profile.user?.id,
    updating: state.posts.updating
  }));
  const [showOwnPosts, setShowOwnPosts] = useState(false);
  const [showLikedPosts, setShowLikedPosts] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [isImageLoading, setIsImageLoading] = useState(false);
  const isFlatListReady = useRef(false); // to avoid firing onEndReached on load on IOS

  const handlePostLike = useCallback(
    id => dispatch(threadActionCreator.likePost(id)),
    [dispatch]
  );

  const handlePostDislike = useCallback(
    id => dispatch(threadActionCreator.dislikePost(id)),
    [dispatch]
  );

  const handlePostDelete = useCallback(
    id => dispatch(threadActionCreator.deletePost(id)),
    [dispatch]
  );

  const handlePostExpand = useCallback(
    id => {
      dispatch(threadActionCreator.loadExpandedPost(id));
      navigation.navigate(HomeScreenName.EXPANDED_POST);
    },
    [dispatch, navigation]
  );

  const handlePostEdit = useCallback(
    post => {
      dispatch({ type: ActionType.UPDATE_POST_PRELOAD, payload: post });
      navigation.navigate(HomeScreenName.EDIT_POST);
    },
    [dispatch, navigation]
  );

  const handlePostShare = useCallback(({ body, image }) => {
    sharePost({ body, image }).catch(() => {
      notificationService.error(NotificationMessage.OPERATION_FAILED);
    });
  }, []);

  const handlePostsLoad = useCallback(
    filtersPayload => dispatch(threadActionCreator.loadPosts(filtersPayload)),
    [dispatch]
  );

  const handleMorePostsLoad = filtersPayload => {
    setIsLoading(true);
    dispatch(threadActionCreator.loadMorePosts(filtersPayload))
      .unwrap()
      .finally(() => {
        setIsLoading(false);
      });
  };

  const handleImageDownload = url => {
    setIsImageLoading(true);
    const imageName = url.substring(url.lastIndexOf('/'));
    const path = templatePath.replace('{imgName}', imageName);

    if (Platform.OS === 'android') {
      RNFetchBlob.config({
        fileCache: true,
        appendExt: 'png',
        path,
        addAndroidDownloads: {
          useDownloadManager: true,
          notification: true,
          description: 'Image',
          path
        }
      }).fetch('GET', url).then(() => {
        setIsImageLoading(false);
      });
    } else {
      CameraRoll.saveToCameraRoll(url).then(() => {
        setIsImageLoading(false);
      });
    }
  };

  const getMorePosts = () => {
    if (isFlatListReady.current && hasMorePosts && !isLoading) {
      handleMorePostsLoad({ ...postsFilter });
      postsFilter.from += postsFilter.count;
    }
  };

  const toggleShowOwnPosts = () => {
    setShowOwnPosts(!showOwnPosts);
    setShowLikedPosts(false);
    postsFilter.userId = showOwnPosts ? undefined : userId;
    postsFilter.isLiked = undefined;
    postsFilter.from = 0;
    setIsLoading(true);
    handlePostsLoad({ ...postsFilter })
      .unwrap()
      .finally(() => setIsLoading(false));
    postsFilter.from += postsFilter.count;
  };

  const toggleShowLikedPosts = () => {
    setShowLikedPosts(!showLikedPosts);
    setShowOwnPosts(false);
    postsFilter.isLiked = showLikedPosts ? undefined : true;
    postsFilter.userId = showLikedPosts ? undefined : userId;
    postsFilter.from = 0;
    setIsLoading(true);
    handlePostsLoad({ ...postsFilter })
      .unwrap()
      .finally(() => setIsLoading(false));
    postsFilter.from += postsFilter.count;
  };

  const handleScroll = () => {
    isFlatListReady.current = true;
  };

  const isPostOwner = postUserId => userId === postUserId;

  useEffect(() => {
    postsFilter.from = 0;

    setIsLoading(true);
    const request = handlePostsLoad({ ...postsFilter });
    request.unwrap().finally(() => setIsLoading(false));

    postsFilter.from += postsFilter.count;

    return () => request.abort();
  }, [handlePostsLoad, setIsLoading]);

  return (
    <FlatList
      bounces={false}
      data={posts}
      keyExtractor={({ id }) => id}
      ListHeaderComponent={(
        <>
          <View style={styles.header}>
            <Icon name={IconName.CAT} size={24} color={AppColor.HEADLINE} />
            <Text variant={TextVariant.HEADLINE} style={styles.logoText}>
              Thread
            </Text>
          </View>
          <View style={styles.filter}>
            <Switch
              value={showOwnPosts}
              label="Show only my posts"
              onToggleValue={toggleShowOwnPosts}
            />
          </View>
          <View style={styles.filter}>
            <Switch
              value={showLikedPosts}
              label="Show only liked posts"
              onToggleValue={toggleShowLikedPosts}
            />
          </View>
        </>
      )}
      onEndReachedThreshold={0.1}
      onEndReached={getMorePosts}
      onScroll={handleScroll}
      renderItem={({ item: post }) => (
        <Post
          post={post}
          updating={updating}
          isPostOwner={isPostOwner(post.userId)}
          onPostLike={handlePostLike}
          onPostDislike={handlePostDislike}
          onPostShare={handlePostShare}
          onPostDelete={handlePostDelete}
          onPostExpand={handlePostExpand}
          onPostEdit={handlePostEdit}
          isImageLoading={isImageLoading}
          onImageDownload={handleImageDownload}
        />
      )}
    />
  );
};

export default Thread;
