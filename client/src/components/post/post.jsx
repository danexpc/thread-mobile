import * as React from 'react';
import PropTypes from 'prop-types';
import { DEFAULT_USER_AVATAR } from 'common/constants/constants';
import { IconName, TextVariant } from 'common/enums/enums';
import { postType } from 'common/prop-types/prop-types';
import { Icon, Image, Stack, Text, View } from 'components/components';
import { getFromNowTime } from 'helpers/helpers';
import styles from './styles';

const Post = ({
  post,
  updating,
  isPostOwner,
  isImageLoading,
  onPostLike,
  onPostDislike,
  onPostDelete,
  onPostShare,
  onPostExpand,
  onPostEdit,
  onImageDownload
}) => {
  const {
    id,
    image,
    body,
    user,
    likeCount,
    dislikeCount,
    commentCount,
    createdAt
  } = post;
  const date = getFromNowTime(createdAt);

  const handlePostLike = () => onPostLike(id);
  const handlePostDislike = () => onPostDislike(id);
  const handlePostExpand = () => onPostExpand(id);
  const handlePostEdit = () => onPostEdit(post);
  const handleImageDownload = () => onImageDownload(image.link);
  const handlePostShare = () => onPostShare({
    body,
    image
  });

  return (
    <View style={styles.container}>
      <Image
        style={styles.avatar}
        accessibilityIgnoresInvertColors
        source={{ uri: user.image?.link ?? DEFAULT_USER_AVATAR }}
      />
      <View style={styles.content}>
        <View style={styles.headerWrapper}>
          <View style={styles.header}>
            <Text variant={TextVariant.TITLE}>{user.username}</Text>
            <Text variant={TextVariant.SUBTITLE}>
              {' • '}
              {date}
            </Text>
          </View>
          {image && (
            <Icon
              name={IconName.DOWNLOAD}
              size={14}
              onPress={isImageLoading ? null : handleImageDownload}
            />
          )}
        </View>
        {image && (
          <View style={styles.header}>
            <Image
              style={styles.image}
              accessibilityIgnoresInvertColors
              source={{ uri: image.link }}
            />
          </View>
        )}
        <Text style={styles.body}>{body}</Text>
        <View style={styles.footer}>
          <Stack space={24} isRow>
            <Icon
              name={IconName.THUMBS_UP}
              size={16}
              label={String(likeCount)}
              onPress={updating ? null : handlePostLike}
            />
            <Icon
              name={IconName.THUMBS_DOWN}
              size={16}
              label={String(dislikeCount)}
              onPress={updating ? null : handlePostDislike}
            />
            <Icon
              name={IconName.COMMENT}
              size={16}
              label={String(commentCount)}
              onPress={onPostExpand ? handlePostExpand : null}
            />
            {
              isPostOwner && (
                <Icon
                  name={IconName.EDIT}
                  size={16}
                  onPress={handlePostEdit}
                />
              )
            }
            {
              isPostOwner && (
                <Icon
                  name={IconName.TRASH}
                  size={16}
                  onPress={() => onPostDelete(id)}
                />
              )
            }
          </Stack>
          <Icon name={IconName.SHARE_ALT} size={16} onPress={handlePostShare} />
        </View>
      </View>
    </View>
  );
};

Post.propTypes = {
  post: postType.isRequired,
  updating: PropTypes.bool.isRequired,
  isPostOwner: PropTypes.bool,
  isImageLoading: PropTypes.bool.isRequired,
  onPostLike: PropTypes.func.isRequired,
  onPostDislike: PropTypes.func.isRequired,
  onPostDelete: PropTypes.func,
  onPostShare: PropTypes.func.isRequired,
  onPostExpand: PropTypes.func,
  onPostEdit: PropTypes.func,
  onImageDownload: PropTypes.func.isRequired
};

Post.defaultProps = {
  onPostExpand: null,
  isPostOwner: false,
  onPostDelete: null,
  onPostEdit: null
};

export default Post;
