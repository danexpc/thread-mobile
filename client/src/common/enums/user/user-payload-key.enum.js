const UserPayloadKey = {
  USERNAME: 'username',
  EMAIL: 'email',
  STATUS: 'status',
  PASSWORD: 'password'
};

export { UserPayloadKey };
