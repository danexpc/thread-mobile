const IconName = {
  CAT: 'cat',
  COMMENT: 'comment',
  ENVELOPE: 'envelope',
  EXCLAMATION_TRIANGLE: 'exclamation-triangle',
  HOME: 'home',
  INFO: 'info',
  INFO_CIRCLE: 'info-circle',
  LOCK: 'lock',
  PAPER_PLANE: 'paper-plane',
  PLUS_SQUARE: 'plus-square',
  SHARE_ALT: 'share-alt',
  THUMBS_UP: 'thumbs-up',
  DOWNLOAD: 'download',
  THUMBS_DOWN: 'thumbs-down',
  USER: 'user',
  TRASH: 'trash',
  EDIT: 'edit'
};

export { IconName };
