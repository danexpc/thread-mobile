import { HttpMethod, ContentType } from 'common/enums/enums';

class User {
  constructor({ apiPath, http }) {
    this._apiPath = apiPath;
    this._http = http;
  }

  updateUserStatus(id, payload) {
    return this._http.load(`${this._apiPath}/users/${id}`, {
      method: HttpMethod.PATCH,
      contentType: ContentType.JSON,
      payload: JSON.stringify(payload)
    });
  }
}

export { User };
