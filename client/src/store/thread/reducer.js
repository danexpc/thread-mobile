import { createReducer, isAnyOf } from '@reduxjs/toolkit';
import * as threadActions from './actions';
import { ActionType } from './common';

const initialState = {
  posts: [],
  expandedPost: null,
  editingPost: null,
  updating: false
};

const reducer = createReducer(initialState, builder => {
  builder.addCase(threadActions.loadPosts.fulfilled, (state, action) => {
    const { posts } = action.payload;

    state.posts = posts;
    state.hasMorePosts = Boolean(posts.length);
  });
  builder.addCase(threadActions.loadMorePosts.pending, state => {
    state.hasMorePosts = null;
  });
  builder.addCase(threadActions.loadMorePosts.fulfilled, (state, action) => {
    const { posts } = action.payload;

    state.posts = state.posts.concat(posts);
    state.hasMorePosts = Boolean(posts.length);
  });
  builder.addCase(threadActions.loadExpandedPost.fulfilled, (state, action) => {
    const { post } = action.payload;

    state.expandedPost = post;
  });
  builder.addCase(threadActions.deletePost.fulfilled, (state, action) => {
    const { posts } = action.payload;

    state.posts = [...posts];
  });
  builder.addCase(ActionType.UPDATE_POST_PRELOAD, (state, action) => {
    state.editingPost = action.payload;
  });
  builder.addCase(threadActions.updatePost.fulfilled, (state, action) => {
    const updatedPost = action.payload;
    if (updatedPost.id) {
      const postIndex = state.posts.findIndex(item => item.id === updatedPost.id);
      const posts = [...state.posts];
      posts[postIndex] = { ...posts[postIndex], ...updatedPost };
      state.posts = posts;
    }
  });
  builder.addMatcher(
    isAnyOf(
      threadActions.likePost.pending,
      threadActions.dislikePost.pending,
      threadActions.addComment.pending
    ),
    state => {
      state.updating = true;
    }
  );
  builder.addMatcher(
    isAnyOf(
      threadActions.likePost.fulfilled,
      threadActions.dislikePost.fulfilled,
      threadActions.addComment.fulfilled
    ),
    (state, action) => {
      const { posts, expandedPost } = action.payload;
      state.posts = posts;
      state.expandedPost = expandedPost;
      state.updating = false;
    }
  );
  builder.addMatcher(
    isAnyOf(
      threadActions.applyPost.fulfilled,
      threadActions.createPost.fulfilled
    ),
    (state, action) => {
      const { post } = action.payload;

      state.posts = [post, ...state.posts];
    }
  );
});

export { reducer };
