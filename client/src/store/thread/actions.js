import { createAsyncThunk } from '@reduxjs/toolkit';
import { ActionType } from './common';

const loadPosts = createAsyncThunk(
  ActionType.SET_ALL_POSTS,
  async (filters, { extra: { services } }) => {
    const posts = await services.post.getAllPosts(filters);
    return { posts };
  }
);

const loadMorePosts = createAsyncThunk(
  ActionType.LOAD_MORE_POSTS,
  async (filters, { getState, extra: { services } }) => {
    const {
      posts: { posts }
    } = getState();
    const loadedPosts = await services.post.getAllPosts(filters);
    const filteredPosts = loadedPosts.filter(
      post => !(posts && posts.some(loadedPost => post.id === loadedPost.id))
    );

    return { posts: filteredPosts };
  }
);

const applyPost = createAsyncThunk(
  ActionType.ADD_POST,
  async (postId, { extra: { services } }) => {
    const post = await services.post.getPost(postId);
    return { post };
  }
);

const createPost = createAsyncThunk(
  ActionType.ADD_POST,
  async (post, { extra: { services } }) => {
    const { id } = await services.post.addPost(post);
    const newPost = await services.post.getPost(id);

    return { post: newPost };
  }
);

const updatePost = createAsyncThunk(
  ActionType.UPDATE_POST,
  async ({ id, payload }, { extra: { services } }) => {
    const updated = await services.post.updatePost(id, payload);

    return updated ? services.post.getPost(id) : {};
  }
);

const loadExpandedPost = createAsyncThunk(
  ActionType.SET_EXPANDED_POST,
  async (postId, { extra: { services } }) => {
    const post = postId ? await services.post.getPost(postId) : undefined;
    return { post };
  }
);

const likePost = createAsyncThunk(
  ActionType.REACT,
  async (postId, { getState, extra: { services } }) => {
    const updatedPost = await services.post.likePost(postId);

    const {
      posts: { posts, expandedPost }
    } = getState();

    const updated = posts.map(post => (post.id !== postId ? post : updatedPost));
    const updatedExpandedPost = expandedPost?.id === postId ? updatedPost : undefined;

    return { posts: updated, expandedPost: updatedExpandedPost };
  }
);

const dislikePost = createAsyncThunk(
  ActionType.REACT,
  async (postId, { getState, extra: { services } }) => {
    const updatedPost = await services.post.dislikePost(postId);

    const {
      posts: { posts, expandedPost }
    } = getState();
    const updated = posts.map(post => (post.id !== postId ? post : updatedPost));
    const updatedExpandedPost = expandedPost?.id === postId ? updatedPost : undefined;

    return { posts: updated, expandedPost: updatedExpandedPost };
  }
);

const deletePost = createAsyncThunk(
  ActionType.DELETE_POST,
  async (postId, { getState, extra: { services } }) => {
    const deleted = await services.post.deletePost(postId);

    const {
      posts: { posts }
    } = getState();

    const afterDeletion = deleted ? posts.filter(post => post.id !== postId) : posts;

    return { posts: afterDeletion };
  }
);

const addComment = createAsyncThunk(
  ActionType.COMMENT,
  async (request, { getState, extra: { services } }) => {
    const { id } = await services.comment.addComment(request);
    const comment = await services.comment.getComment(id);

    const mapComments = post => ({
      ...post,
      commentCount: Number(post.commentCount) + 1,
      comments: [...(post.comments || []), comment] // comment is taken from the current closure
    });

    const {
      posts: { posts, expandedPost }
    } = getState();
    const updated = posts.map(post => (post.id !== comment.postId ? post : mapComments(post)));

    const updatedExpandedPost = expandedPost?.id === comment.postId
      ? mapComments(expandedPost)
      : undefined;

    return { posts: updated, expandedPost: updatedExpandedPost };
  }
);

export {
  loadPosts,
  loadMorePosts,
  applyPost,
  createPost,
  updatePost,
  loadExpandedPost,
  likePost,
  dislikePost,
  deletePost,
  addComment
};
