const ActionType = {
  ADD_POST: 'thread/add-post',
  UPDATE_POST_PRELOAD: 'thread/edit-post-preload',
  UPDATE_POST: 'thread/edit-post',
  LOAD_MORE_POSTS: 'thread/load-more-posts',
  SET_ALL_POSTS: 'thread/set-all-posts',
  SET_EXPANDED_POST: 'thread/set-expanded-post',
  REACT: 'thread/react',
  DELETE_POST: 'thread/delete-post',
  COMMENT: 'thread/comment'
};

export { ActionType };

