const ActionType = {
  LOG_IN: 'profile/log-in',
  LOG_OUT: 'profile/log-out',
  REGISTER: 'profile/register',
  UPDATE_STATUS: 'profile/update-status'
};

export { ActionType };
