class User {
  constructor({ userRepository }) {
    this._userRepository = userRepository;
  }

  getUserById(id) {
    return this._userRepository.getUserById(id);
  }

  updateUserStatusById(id, { status = null }) {
    return this._userRepository.updateUserStatusById(id, status);
  }
}

export { User };
