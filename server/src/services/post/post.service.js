import PostReaction from '../../data/models/post-reaction/post-reaction.model';

class Post {
  constructor({ postRepository, postReactionRepository }) {
    this._postRepository = postRepository;
    this._postReactionRepository = postReactionRepository;
  }

  getPosts(filter) {
    if (filter.isLiked !== undefined) {
      return this._postRepository.getPostsFilteredByUserLike(filter);
    }
    return this._postRepository.getPostsFilteredByUser(filter);
  }

  getPostById(id) {
    return this._postRepository.getPostById(id);
  }

  create(userId, post) {
    return this._postRepository.create({
      ...post,
      userId
    });
  }

  deleteById(id) {
    return PostReaction.transaction(async trx => {
      await this._postReactionRepository.deletePostReactionByPostId(id)
        .transacting(trx);
      return this._postRepository.deleteById(id)
        .transacting(trx);
    });
  }

  async setReaction(userId, { postId, isLike = true }) {
    // define the callback for future use as a promise
    const updateOrDelete = react => (react.isLike === isLike
      ? this._postReactionRepository.deleteById(react.id)
      : this._postReactionRepository.updateById(react.id, { isLike }));

    const reaction = await this._postReactionRepository.getPostReaction(
      userId,
      postId
    );

    if (reaction) await updateOrDelete(reaction);
    else await this._postReactionRepository.create({ userId, postId, isLike });

    return this._postRepository.getPostById(postId);
  }
}

export { Post };
