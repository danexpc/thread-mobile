import { UsersApiPath } from '../../common/enums/enums';

const initUser = (router, opts, done) => {
  const { user: userService } = opts.services;

  router
    .patch(UsersApiPath.$ID, req => userService.updateUserStatusById(req.params.id, req.body));

  done();
};

export { initUser };
